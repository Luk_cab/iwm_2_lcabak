package networking.udp;
 
import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.nio.file.Files;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.Packet;

public class UDPServer {
	
    public static void main(String[] args) throws Exception {

    	System.out.println("Program Started");
    	DatagramSocket aSocket = null;
    	
      try {
    	  
        aSocket = new DatagramSocket(9900);
        byte[] buffer = new byte[1024];
        
        while(true) {
        	
          // RECEIVE REQUEST
          DatagramPacket request = new DatagramPacket(buffer, buffer.length);
          System.out.println("Waiting for request...");
          aSocket.receive(request);
          
          Request read = (Request) Tools.deserialize(request.getData());
          System.out.println("Received request \n");
		
		  String feedback = "";
		  File dir = new File("/Users/workspace/test/IWM_Cwiczenie_2/");
		  File[] files = null;
		  
		  switch (read.getType()){
			
		  
			case Save:

				Packet receivedObject = (Packet) read.getObject();
				Files.write(new File(dir.getPath()).toPath(), Tools.serialize(receivedObject));
				feedback = new String("Server received and saved object");

				break;

			case List:

		        files = dir.listFiles();
		        feedback = "\n All files: \n";
		        if (files != null && files.length > 0) {
		            for (File file : files) {
		            	if (file.isFile() && !file.getName().startsWith(".")) {
		                    feedback = feedback + "|--" +file.getName() + "\n";
		                }
		            }
		        }
				
				break;
				
			case Search:
			
				files = dir.listFiles((dir1, name) -> name.toLowerCase().contains(read.getValue().toLowerCase().replaceAll("\\s+","_")) );
				
		        if (files != null && files.length > 0) {
		            for (File file : files) {
		                    feedback = feedback + "|--" +file.getName() + "\n";		                
		            }
		        } else {
		        	feedback = "Didn't find any file that fulfils the criteria";
		        }
				break;
				
			case Get:
				
				File file = new File(read.getValue());
				if (file.exists()) {
					byte[] data = Files.readAllBytes(file.toPath());
					feedback = "Received info from the server: \n" + Tools.deserialize(data).toString();
				}else {
					feedback = "There is no such file";
				}
				
				break;
		
			default:
				
				feedback = "Provide right type";
				
				break;
			
			}
			
		  //Send replay
		  
			DatagramPacket reply = new DatagramPacket(feedback.getBytes(), feedback.length(), 
					request.getAddress(), request.getPort());
			aSocket.send(reply); 
          
        }
        
        // Exceptions
      } catch (SocketException ex) {
        Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
        Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
      } finally {
				aSocket.close();
		    	System.out.println("Socket closed");
			}
    }
}

