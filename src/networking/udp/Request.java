package networking.udp;

import java.io.Serializable;

import main.Packet;
import networking.udp.Request.Type;

public class Request extends Packet implements Serializable {

	public enum Type {
		Save, Search, List, Get
	}
	
	private Type type; 
	private String value;
	private Object object;

	Request(){
		super();
		value = "";
		type = Type.List;
		object = null;
	}
	
	Request(String device, String description, long date, Type type, String value, Object object){
		super(device, description, date);
		this.type = type;
		this.value = value;
		this.object = object;
	}
	
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
	public Object getObject() {
		return object;
	}

	public void setObject(Object object) {
		this.object = object;
	}
	
public String toString() {
    	
    	String allValues;
    	
    	allValues = super.toString() + ", type = " + type + ", value = " + value + ", object = " + "[ " + object + " ]";
    			
        return allValues;
    }
}
