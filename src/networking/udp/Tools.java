package networking.udp;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.Vector;


public class Tools {

	
	public Tools() {}
	
	private boolean serialize(Object obj, String path) {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(path));
			out.writeObject(obj);
			out.close();
			System.out.println("Serialized data is saved in " + path);
			return true;
		} catch (IOException i) {
			i.printStackTrace();
			return false;
		}
	}

	private Object deserialize(String path) throws ClassNotFoundException {
		Object obj = null;
		try {
			ObjectInputStream in = new ObjectInputStream(new FileInputStream(path));
			obj = in.readObject();
			in.close();
			System.out.println("Serialized data is retrieved from " + path);
			return obj;
		} catch (IOException i) {
			i.printStackTrace();
			return obj;
		}
	}
	

	public static byte[] serialize(Object obj) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		ObjectOutput out = null;
		try {
		  out = new ObjectOutputStream(bos);   
			out.writeObject(obj);
		  out.flush();
		  return bos.toByteArray();
		}
		catch(IOException e) {
			e.printStackTrace();
			return null;
		}
		finally {
		  try {
		    bos.close();
		  } catch (IOException e) {
			  e.printStackTrace();
		  }
	  }
	}
	

	public static Object deserialize(byte[] bytes) throws ClassNotFoundException {
		Object obj = null;
		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
			ObjectInputStream in = new ObjectInputStream(bis);
			obj = in.readObject();
			in.close();
			System.out.println("Serialized data is retrieved from bytes array");
			return obj;
		} catch (IOException e) {
			e.printStackTrace();
			return obj;
		}
	}

	// serialize an array of Strings to a byte array 
	void serializeStringsByteArray() {
		String[] names = { "First", "Second", "Third" };
		byte[] bytes = serialize(names);
		if (bytes != null) {
			String[] rd = null;
			try {
				rd = (String[]) deserialize(bytes);
				for (String s : rd)
					System.out.println(s);
			} catch (ClassNotFoundException c) {
				System.out.println("data not found");
				c.printStackTrace();
			}
		}
	}


	// serialize an array of Strings to a file  
	void serializeStrings() {
		String[] names = { "First", "Second", "Third" };
		String path = "./Object.ser";
		if (serialize(names, path) == true) {
			String[] rd = null;
			try {
				rd = (String[]) deserialize(path);
				for (String s : rd)
					System.out.println(s);
			} catch (ClassNotFoundException c) {
				System.out.println("data not found");
				c.printStackTrace();
			}
		}
	}

	// serialize a Vector to a byte array 
	void serializeVectorByteArray() {		
		Vector v = new Vector();
		v.addElement(new Integer(1));
		v.addElement(Integer.valueOf(2));
		v.addElement(Float.valueOf(3.14f));
		v.addElement(Double.valueOf(3.1415d));
		byte[] bytes = serialize(v);
		if (bytes != null) {
			Vector rd;
			try {
				rd = (Vector) deserialize(bytes);
				System.out.println(rd);
			} catch (ClassNotFoundException c) {
				System.out.println("data not found");
				c.printStackTrace();
			}
		}
	}
	
	// serialize a Vector to a file
	void serializeVector() {
		Vector v = new Vector();
		v.addElement(new Integer(1));
		v.addElement(Integer.valueOf(2));
		v.addElement(Float.valueOf(3.14f));
		v.addElement(Double.valueOf(3.1415d));
		String path = "./Object.ser";
		if (serialize(v, path) == true) {
			Vector rd = null;
			try {
				rd = (Vector) deserialize(path);
				System.out.println(rd);
			} catch (ClassNotFoundException c) {
				System.out.println("data not found");
				c.printStackTrace();
			}
		}
	}
}
