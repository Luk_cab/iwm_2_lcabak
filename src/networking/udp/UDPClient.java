package networking.udp; 
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.Packet;
import main.TimeHistory;

import networking.udp.Request.Type;


public class UDPClient {
	
	 static DatagramSocket aSocket = null;
	 static int serverPort = 9900;	
	 
	public static void main(String[] args) throws Exception 
	{
		System.out.println("Program Started");

		TimeHistory packet = new TimeHistory("Device2", "Setsignal", 100, 1, "try", 0.2, null,1);
		
		Request request = new Request();
		request.setType(Type.Save);
		request.setValue("Test.txt");
		request.setObject(packet);
		
		send(request);
	}
	
	public static void send(Object object) {
		
		try {
			
			byte[] data = Tools.serialize(object);
			byte[] receivedData = new byte[1024];

			InetAddress aHost = InetAddress.getLocalHost();

			aSocket = new DatagramSocket();

			DatagramPacket request = new DatagramPacket(data, data.length, aHost, serverPort);
			aSocket.send(request); 
			
	        System.out.println("...sending data...");

			DatagramPacket reply = new DatagramPacket(receivedData, receivedData.length);
	        aSocket.setSoTimeout(1000);
			aSocket.receive(reply);					
	        
	        String t = new String(reply.getData(), 0, reply.getLength());
	        System.out.println(t);

		} catch (SocketException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
			System.out.println("Socket closed");
		}
		
	}
}